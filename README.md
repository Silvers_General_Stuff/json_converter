### About
React Component.

These past few months I have tried quite a few packages for react to convert json to csv, but none of them functioned the way I wanted.  
I wanted a way to convert json, preserving numbers as well as the ability to specify custom delimiters.  
None seemed to have this functionality.  
So like all ~~good~~ projects I decided at 2am to create my own.  
I think it turned out reasonable.  
Returns a link to download the resulting file.

### Usage

##### Installation
``yarn add silvers_json_converter``  
``npm install silvers_json_converter``

##### Importing 
``import JSONConverter from "silvers_json_converter"``

### Props
#### data
*Required*.  
Type: array.  
This takes an array of objects.

```
let inputData=[
  {id:1,name:"First Item"},
  {id:2,name:"Second Item"}
]
```

#### type
Type: string.  
This decides the output type.  
Defaults to ``CSV``.

``let outputType = "CSV``  
``let outputType = "JSON``

#### headers
Type: array.  
This takes an array of objects.  
If not specified it defaults to all fields from all elements in the ``data`` prop. 

##### key
If ``headers`` is specified but ``key`` is not then this field is skipped.

##### title 
If ``title`` is not specified it is ``key`` with first letter capitalised.  

##### type
Type of data in that col.  
If not specified it defaults to ``string``  .
Can optionally be ``number``.


```
let headers = [
  {title:"ID",key:"id",type:"number"},
  {title:"Name",key:"name",type:"string"}
]
```

#### separator
Type: string.  
CSV only.  
This chooses what the col divider is.  
If not specified it defaults to ``,``

#### beautify
Type: boolean.  
JSON only.  
If true it outputs the JSON as human readable.  
If not specified it defaults to minified version.


#### name
Type: string.  
Name of output file.  
If not specified it defaults to current date in ISO format.


#### text
Text to display on the link.  
If not specified it defaults to ``Download CSV`` or ``Download JSON`` depending on ``type``.


### Examples

````
<JSONConverter
  data={inputData}
  type={"JSON"}
  beautify={true}
  headers={headers}
  text={"JSON: Expanded"}
/>

<JSONConverter
  data={inputData}
  type={"JSON"}
  beautify={false}
  headers={headers}
  text={"JSON: Compressed"}
/>

<JSONConverter
  data={inputData}
  type={"CSV"}
  headers={headers}
  separator={","}
  text={"CSV: Comma"}
/>

<JSONConverter
  data={inputData}
  type={"CSV"}
  headers={headers}
  separator={";"}
  text={"CSV: Semicolon"}
/>

<JSONConverter
  data={inputData}
  type={"CSV"}
  headers={headers}
  separator={"\t"}
  text={"CSV: Tab"}
/>
````

### Misc
#### Improve this, if you can.
If you think ye can do it better please fork it.  
I have tried to make it as robust as possible for my own usage.

#### Contact 
I am on Discord ``@Silver#5563``