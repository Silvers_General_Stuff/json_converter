import React, { Component } from 'react'

function capitalizeFirstLetter (string) { return string.charAt(0).toUpperCase() + string.slice(1) }

function beautifyJSON (body, flag) {
  if (flag === 'min') {
    body = JSON.stringify(body)
  }
  if (flag !== 'min') {
    body = JSON.stringify(body, null, '  ')
  }
  return body
}

class JSONConverter extends Component {
  constructor (props) {
    super(props)

    this.state = {
      data: props.data,
      type: props.type,
      headers: props.headers,
      separator: props.separator,
      beautify: props.beautify,
      name: props.name,
      text: props.text,
      excel:props.excel
    }
  }

  componentWillReceiveProps (nextProps) {
    let tmp = {}
    tmp.data = nextProps.data
    tmp.type = nextProps.type
    tmp.headers = nextProps.headers
    tmp.separator = nextProps.separator
    tmp.beautify = nextProps.beautify
    tmp.name = nextProps.name
    tmp.text = nextProps.text
    tmp.excel = nextProps.excel
    this.setState(tmp)
  }

  static processHeaders (headers, data) {
    if (typeof headers === 'undefined') {
      let headersTmp = {}
      for (let i = 0; i < data.length; i++) {
        let currentHeaders = Object.keys(data[i])
        for (let j = 0; j < currentHeaders.length; j++) {
          if (typeof headersTmp[currentHeaders[j]] === 'undefined') {
            headersTmp[currentHeaders[j]] = ''
          }
        }
      }
      headers = []
      headersTmp = Object.keys(headersTmp)
      for (let i = 0; i < headersTmp.length; i++) {
        headers.push({ title: capitalizeFirstLetter(headersTmp[i]), key: headersTmp[i], type: 'string' })
      }
    }
    let headersTmp = headers
    headers = []
    for (let i = 0; i < headersTmp.length; i++) {
      if (typeof headersTmp[i].key === 'undefined') {
        continue
      }
      if (typeof headersTmp[i].title === 'undefined') {
        headersTmp[i].title = capitalizeFirstLetter(headersTmp[i].key)
      }
      if (typeof headersTmp[i].type === 'undefined') {
        headersTmp[i].type = 'string'
      }
      headers.push(headersTmp[i])
    }

    return headers
  }

  static processCSV (data, headers, separator, excel) {
    if (typeof separator === 'undefined') {
      separator = ','
    }

    if (typeof headers === 'undefined') {
      headers = JSONConverter.processHeaders(data)
    }

    let csvHeadersTmp = []
    let headersKeyed = {}
    for (let i = 0; i < headers.length; i++) {
      csvHeadersTmp.push(headers[i].title)
      headersKeyed[headers[i].key] = headers[i]
    }

    let csv = "";
    if(excel){
      csv += "sep=" + separator + '\r\n'
    }

    csv += csvHeadersTmp.join(separator) + '\r\n'
    for (let i = 0; i < data.length; i++) {
      let tmp = ''
      for (let j = 0; j < headers.length; j++) {
        let item = data[i][headers[j].key]
        if (typeof item !== 'undefined') {
          if (headers[j].type === 'number') {
            if (!isNaN(item - 0)) {
              item = item - 0
            }
          }
          if (headers[j].type === 'string') {
            if (item.indexOf(separator) !== -1) {
              item = '"' + item + '"'
            }
          }
          tmp += item
        }
        if (j < headers.length - 1) {
          tmp += separator
        }
      }
      tmp += '\r\n'
      csv += tmp
    }
    return 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv)
  }

  static processJSON (data, headers, beautify) {
    if (typeof headers === 'undefined') {
      headers = JSONConverter.processHeaders(data)
    }
    let result = []
    for (let i = 0; i < data.length; i++) {
      let tmp = {}
      for (let j = 0; j < headers.length; j++) {
        let item = data[i][headers[j].key]
        if (typeof item !== 'undefined') {
          if (headers[j].type === 'number') {
            if (!isNaN(item - 0)) {
              item = item - 0
            }
          }
          let field = headers[j].title
          if (typeof field === 'undefined') { field = headers[j].key }
          tmp[field] = item
        }
      }
      result.push(tmp)
    }

    let output
    if (beautify) {
      output = beautifyJSON(result, 'human')
    } else {
      output = JSON.stringify(result)
    }
    return 'data:text/json;charset=utf-8,' + encodeURIComponent(output)
  }

  render () {
    if (
      typeof this.state.data === 'undefined' ||
      !Array.isArray(this.state.data) ||
      this.state.data.length === 0
    ) {
      return <div />
    }

    let type = this.state.type
    let name = this.state.name
    let text = this.state.text

    if (typeof type === 'undefined') { type = 'CSV' }
    if (typeof name === 'undefined') { name = new Date().toISOString() }
    if (typeof text === 'undefined') { text = 'Download ' + type }

    let output
    if (type === 'CSV') {
      output = JSONConverter.processCSV(this.state.data, this.state.headers, this.state.separator, this.state.excel)
      name = name + '.csv'
    }
    if (type === 'JSON') {
      output = JSONConverter.processJSON(this.state.data, this.state.headers, this.state.beautify)
      name = name + '.json'
    }
    return <a href={output} target={'_blank'} download={name} >{text}</a>
  }
}

export default JSONConverter
